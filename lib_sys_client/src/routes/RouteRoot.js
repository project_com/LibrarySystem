import axios from 'axios'
import { useEffect, useState } from 'react'
import { Route, Routes } from 'react-router-dom'
import { StyledContainer } from '../components/Style'
import ProtectedRoutes from '../loginAuth/ProtectedRoutes'
import Order from '../Order/Order'
import Books from '../pages/book/Books'
import CheckInfo from '../pages/CheckInfo'
import Edit from '../pages/Edit'
import LoginView from '../pages/LoginView'
import ManagerView from '../pages/ManagerView'
import Register from '../pages/Register'
import RenewPasswordView from '../pages/RenewPasswordView'
import Rental from '../pages/Rental/Rental'
import SendMail from '../pages/SendMail'
import UserView from '../pages/UserView'
import ManagerReserve from '../pages/manager/ManagerReserve'
import ManagerRental from '../pages/manager/ManagerRental'
import EditPersonal from '../pages/EditPersonal'

function RouteRoot() {
  const [authCode, setAuthCode] = useState(2)
  const loadCode = async () => {
    const response = await axios.get('http://localhost:3001/getAuthCode')
    setAuthCode(response.data.data)
  }
  useEffect(() => {
    loadCode()
  }, [])

  let isLogged = authCode === 2 ? false : true
  return (
    <div>
      <StyledContainer>
        <Routes>
          <Route index path="/" element={<LoginView />}></Route>

          <Route element={<ProtectedRoutes isLogged={isLogged} />}>
            <Route path="/userview" element={<UserView />} exact />
            <Route path="manager" element={<ManagerView />} />
            <Route path="book" element={<Books />} />
            <Route path="register" element={<Register />} />
            <Route path="edit" element={<Edit />} />
            <Route path="edit-personal" element={<EditPersonal />} />
            <Route path="rental" element={<Rental />} />
            <Route path="order" element={<Order />} />
            <Route path="manager-reserve" element={<ManagerReserve />} />
            <Route path="manager-rental" element={<ManagerRental />} />
          </Route>

          <Route path="resetinfo" element={<SendMail />}></Route>
          <Route path="info" element={<CheckInfo />}></Route>
          <Route path="passchange" element={<RenewPasswordView />}></Route>
        </Routes>
      </StyledContainer>
    </div>
  )
}

export default RouteRoot
