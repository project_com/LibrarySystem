import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reduceres/rootReducer';
import { sessionService } from 'redux-react-session';

const initalState = {};
const middilewares = [thunk];

const store = createStore(rootReducer, initalState, compose(applyMiddleware(...middilewares)));

sessionService.initSessionService(store);

export default store;