import logger from 'pino';

//ログのレベルを分けて、日時も表示
const log = logger({
    base: { pid: false },
    transport: {
        target: 'pino-pretty',
        options: {
            colorized: true
        },
        timestamp: () => `, "time": "${new Date().toLocaleString()}"`
    }
});

export default log;