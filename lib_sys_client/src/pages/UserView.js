import React, { useEffect, useState } from 'react'
import {
  StyledFormButton,
  StyledUserButton,
  CopyrightText,
} from './../components/Style'

import { useNavigate, Link } from 'react-router-dom'
import axios from 'axios'

const UserView = () => {
  const navigate = useNavigate()
  const [authorCODE, setAuthorCODE] = useState()

  const loadAuthorCODE = async () => {
    const response = await axios.get('http://localhost:3001/getAuthCode')
    setAuthorCODE(response.data.data)
  }
  useEffect(() => {
    loadAuthorCODE()
  }, [])

  const handleLogout = () => {
    var r = window.confirm('ログアウトしてもよろしいですか?')
    if (r) {
      axios.get('http://localhost:3001/resetID')
      navigate('/')
    }
  }

  return (
    <div>
      {/**ページタイトル */}
      <h1
        className="mx-auto flex flex col justify-center items-center"
        style={{
          fontSize: 80,
        }}
      >
        マイライブラリ
      </h1>

      {/**ログアウトのボタンをセッティング
       * クリックしたらログインページに遷移
       */}
      <div
        style={{
          position: 'absolute',
          top: 10,
          right: 20,
          backgroundColor: 'transparent',
          width: '100%',
          padding: '15px',
          display: 'flex',
          justifyContent: 'flex-end',
        }}
      >
        <StyledFormButton onClick={handleLogout}>ログアウト</StyledFormButton>
      </div>
      {/**管理者ページへ遷移のボタンをセッティング*/}
      {authorCODE === 1 && (
        <div
          style={{
            position: 'absolute',
            top: 70,
            right: 20,
            backgroundColor: 'transparent',
            width: '100%',
            padding: '15px',
            display: 'flex',
            justifyContent: 'flex-end',
          }}
        >
          <Link to="/manager">
            <StyledFormButton>管理者画面</StyledFormButton>
          </Link>
        </div>
      )}

      {/** ページのコンテント */}
      <div className="min-h-screen">
        <div className="max-w-5xl mx-auto flex flex col justify-center items-center">
          <StyledUserButton>図書検索・予約</StyledUserButton>
          <br />
          <Link to="/edit-personal">
            <StyledUserButton>個人情報変更</StyledUserButton>
          </Link>
          <br />
          <Link to="/passchange">
            <StyledUserButton>パスワード変更</StyledUserButton>
          </Link>
          <br />

          <Link to="/order">
            <StyledUserButton
              style={{
                width: '40%',
                float: 'left',
              }}
            >
              予約状況
            </StyledUserButton>
          </Link>
          <Link to="/rental">
            <StyledUserButton
              style={{
                width: '40%',
                float: 'right',
                marginRight: 0,
              }}
            >
              貸出状況
            </StyledUserButton>
          </Link>
        </div>
      </div>
      <div
        style={{
          bottom: 0,
          clear: 'right',
        }}
      >
        <CopyrightText>All rights reserved &copy;2022</CopyrightText>
      </div>
    </div>
  )
}

export default UserView
