import { Link } from 'react-router-dom'
import { StyledFormButton } from '../Style.js'

function ButtonRender({ data }) {
  return (
    <>
      <Link onClick={data.logout} to={data.to}>
        <StyledFormButton
          style={{ fontSize: '15px', width: '120px', padding: '5px' }}
        >
          {data.title}
        </StyledFormButton>
      </Link>{' '}
      <br />
    </>
  )
}

export default ButtonRender
