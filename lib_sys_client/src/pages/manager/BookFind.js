import axios from "axios";
import React, { useRef, useState } from "react";
import ReactDOM from "react-dom";

// import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    CLOSE_BUTTON,
    MODAL_STYLES,
    OVERLAY_STYLES,
} from "../../components/Style";
import InfoRentalContent from "./InfoRentalContent";

export default function BookFind({ show, close }) {
    const inputValue = useRef(null);
    const [data, setData] = useState([]);

    const loadData = async () => {
        setIsOpen(true);
        const bookID = inputValue.current.value;
        const response = await axios.get(
            `http://localhost:3001/book-detail-for-rental/${bookID}`
        );
        console.log(response.data);
        setData(response.data);
    };

    const [isOpen, setIsOpen] = useState(false);

    if (!show) return;
    return ReactDOM.createPortal(
        <OVERLAY_STYLES>
            <MODAL_STYLES>
                <div>
                    <label htmlFor="site-search">書籍ID:</label>
                    <input ref={inputValue} type="search" id="site-search" />

                    <button onClick={() => loadData()}>Search</button>
                </div>
                <InfoRentalContent open={isOpen} data={data} />
                <CLOSE_BUTTON onClick={close}>X</CLOSE_BUTTON>
            </MODAL_STYLES>
        </OVERLAY_STYLES>,
        document.getElementById("portal")
    );
}
