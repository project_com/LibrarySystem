import * as Yup from 'yup'
import 'yup-phone'

// 個人情報修正フォームのバリデーション
export const personalSchema = Yup.object().shape({
  email: Yup.string()
    .max(50, '50文字以下を入力してください。')
    .email('正しいメールアドレスを入力してください。')
    .required('メールアドレスを入力してください。'),
  phone: Yup.string()
    .phone('JP', true, '正しい電話番号を入力してください。')
    .required('電話番号を入力してください。'),
  postCode: Yup.string()
    .matches(/^\d{7}$/, '正しい郵便番号を入力してください。')
    .required('郵便番号を入力してください。'),
  address: Yup.string()
    .required('住所を入力してください。')
    .max(70, '70文字以下を入力してください。')
    .matches(/^[^\x20-\x7e]*$/, '全角文字のみ'),
})
