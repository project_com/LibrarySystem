// ライブラリー
import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
// 自作コンポーネント
import UserFormFooter from '../components/form-user/UserFormFooter'
import UserConfirmModal from '../components/form-user/UserConfirmModal'
import { personalSchema } from '../components/form-user/PersonalValidation'
import { formatOutputBirthday } from '../components/form-user/DatePicker'
import PersonalFormList from '../components/form-user/PersonalFormList'
import { getLoginID } from '../components/getLoginID'

// 会員情報修正画面
export default function EditPersonal() {
  const [userID, setUserID] = useState()
  const [name, setName] = useState('')
  const [nameKana, setNameKana] = useState('')
  const [birthday, setBirthday] = useState('')
  const [gender, setGender] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [postCode, setPostCode] = useState('')
  const [address, setAddress] = useState('')
  const [authorityCODE, setAuthorityCODE] = useState()
  // 画面遷移 hook
  const navigate = useNavigate()
  // react hook form
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(personalSchema),
  })

  // 画面起動時１回だけログインID取得、会員情報取得
  useEffect(() => {
    async function fetchLoginID() {
      // getLoginID: カスタム hook
      const loginID = await getLoginID()
      console.log('login ID:', loginID)
      setUserID(loginID)
      getUser(loginID)
    }
    fetchLoginID()
    // eslint-disable-next-line
  }, [])
  // 選択した会員情報をDBから取得する関数
  const getUser = (userID) => {
    axios.get(`http://localhost:3001/get/${userID}`).then((response) => {
      const r = response.data[0]
      setName(r.name)
      setNameKana(r.name_kana)
      setBirthday(formatOutputBirthday(r.birthday))
      setGender(r.gender)
      setEmail(r.email)
      setPhone(r.phone)
      setPostCode(r.postCode)
      setAddress(r.address)
      setAuthorityCODE(r.authority_CODE)
      reset()
    })
  }
  // 修正した会員情報をDBにアップデートする関数
  const updUser = () => {
    axios
      .put('http://localhost:3001/user/edit/update-personal', {
        id: userID,
        email: email,
        phone: phone,
        postCode: postCode,
        address: address,
        updID: userID,
      })
      .then((response) => {
        const r = response.data.message
        // DBエラーメッセージ表示
        if (r === 'updated') {
          alert('修正完了')
          navigate('/userview')
        } else {
          if (r === 'dup email') {
            alert('メールアドレスが重複しています。')
          } else if (r === 'dup phone') {
            alert('電話番号が重複しています。')
          } else {
            alert(
              '再度ログインしてもできない場合は、担当者までお問い合わせください。'
            )
          }
          // モーダルを閉じる
          handleClose()
          // 確定ボタンを再活性化
          handleActive()
        }
      })
  }

  //モーダル制御
  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  //確定ボタン制御
  const [active, setActive] = useState(false)
  const handleInactive = () => setActive(true)
  const handleActive = () => setActive(false)

  // submit時の処理
  const onSubmit = (input) => {
    console.log(input)
    setEmail(input.email)
    setPhone(input.phone)
    setPostCode(input.postCode)
    setAddress(input.address)
    // モーダルを開く
    handleShow()
  }

  return (
    <div className="card my-5 mx-auto" style={{ width: '25rem' }}>
      <div className="card-body">
        {/* フォームヘッダー */}
        <h4 className="mt-1">個人情報修正</h4>
        <div
          className="border-bottom mt-3 mb-2"
          style={{ margin: '-16px' }}
        ></div>
        <form
          // ブラウザーの基本バリデーション非活性化
          noValidate
          // handleSubmitでバリデーション ➔ モーダル表示
          onSubmit={handleSubmit((input) => {
            onSubmit(input)
          })}
        >
          {/* フォームリスト */}
          <PersonalFormList
            register={register}
            errors={errors}
            name={name}
            nameKana={nameKana}
            birthday={birthday}
            gender={gender}
            email={email}
            phone={phone}
            postCode={postCode}
            address={address}
          />
          {/* フォームフッター */}
          <UserFormFooter
            getUser={() => getUser(userID)}
            navigate={navigate}
            navigatePath={'/userview'}
          />
        </form>
        {/* 確認モーダル */}
        <UserConfirmModal
          modalName="個人情報修正確認"
          // updUser: 修正関数
          updUser={updUser}
          show={show}
          handleClose={handleClose}
          id={userID}
          name={name}
          nameKana={nameKana}
          birthday={birthday}
          gender={gender}
          email={email}
          phone={phone}
          postCode={postCode}
          address={address}
          authorityCODE={authorityCODE}
          active={active}
          handleInactive={handleInactive}
        />
      </div>
    </div>
  )
}
