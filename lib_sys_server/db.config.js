import mysql from 'mysql2'
import dotenv from 'dotenv'

dotenv.config()
//MySQL　のデータベースにコネクションの作成
const db = mysql.createPool({
  host: 'localhost',
  user: 'test',
  password: '123456',
  database: 'lib_sys',
  multipleStatements: true,
})

// const db = mysql.createPool({
//   host: 'localhost',
//   user: 'vi',
//   password: 'h.123456',
//   database: 'sysdb',
//   multipleStatements: true,
// })

export default db
