// ライブラリー
import React, { useState, useEffect } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import axios from 'axios'
import { useNavigate } from 'react-router-dom'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
// 自作コンポーネント
import UserFormList from '../components/form-user/UserFormList'
import UserFormFooter from '../components/form-user/UserFormFooter'
import UserConfirmModal from '../components/form-user/UserConfirmModal'
import { getLoginID } from '../components/getLoginID'
import { userSchema } from '../components/form-user/UserValidation'
import { formatInputBirthday } from '../components/form-user/DatePicker'
import AuthBox from '../components/form-user/AuthBox'

// 会員情報登録画面
export default function RegisterUser() {
  //取得したログインID変数
  const [regID, setRegID] = useState()
  //入力情報変数
  const [userID, setUserID] = useState()
  const [password, setPassword] = useState('')
  const [name, setName] = useState('')
  const [nameKana, setNameKana] = useState('')
  const [birthday, setBirthday] = useState('')
  const [gender, setGender] = useState('m')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [postCode, setPostCode] = useState('')
  const [address, setAddress] = useState('')
  const [authorityCODE, setAuthorityCODE] = useState(false)
  // 画面遷移 hook
  const navigate = useNavigate()
  // react hook form
  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(userSchema),
  })

  // 画面起動時１回だけログインID取得
  useEffect(() => {
    async function fetchLoginID() {
      // getLoginID: カスタム hook
      const loginID = await getLoginID()
      setRegID(loginID)
      console.log('login ID:', loginID)
    }
    fetchLoginID()
  }, [])

  //入力した会員情報をDBに登録する関数
  const addUser = () => {
    axios
      .post('http://localhost:3001/register', {
        id: userID,
        password: password,
        name: name,
        nameKana: nameKana,
        birthday: birthday,
        gender: gender,
        email: email,
        phone: phone,
        postCode: postCode,
        address: address,
        authorityCODE: authorityCODE,
        regID: regID,
      })
      .then((response) => {
        // DBエラーメッセージ表示
        const r = response.data.message
        if (r === 'inserted') {
          alert('登録完了')
          // 管理者画面に遷移
          navigate('/manager')
        } else {
          if (r === 'dup id') {
            alert('IDが重複しています。')
          } else if (r === 'dup email') {
            alert('メールアドレスが重複しています。')
          } else if (r === 'dup phone') {
            alert('電話番号が重複しています。')
          } else {
            alert(
              '再度ログインしもできない場合は、担当者までお問い合わせください。'
            )
          }
          // モーダルを閉じる
          handleClose()
          // 確定ボタンを再活性化
          handleActive()
        }
      })
  }

  //モーダル制御
  const [show, setShow] = useState(false)
  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  //確定ボタン制御
  const [active, setActive] = useState(false)
  const handleInactive = () => setActive(true)
  const handleActive = () => setActive(false)

  // submit時の処理
  const onSubmit = (input) => {
    console.log(input)
    // ID: 6桁のランダム変数（100000～999999）
    setUserID(Math.floor(Math.random() * 900000) + 100000)
    // 生年月日の連番をパスワードに設定
    setPassword(formatInputBirthday(input.birthday).replace(/-/g, ''))
    setName(input.name)
    setNameKana(input.nameKana)
    setBirthday(formatInputBirthday(input.birthday))
    setGender(input.gender)
    setEmail(input.email)
    setPhone(input.phone)
    setPostCode(input.postCode)
    setAddress(input.address)
    // checked(true): 1(管理者), unchecked(false): 0(一般会員)
    setAuthorityCODE(input.authorityCODE ? 1 : 0)
    // モーダルを開く
    handleShow()
  }

  return (
    <div className="card my-5 mx-auto" style={{ width: '25rem' }}>
      <div className="card-body">
        {/* フォームヘッダー */}
        <h4 className="mt-1">会員情報登録</h4>
        <div
          className="border-bottom mt-3 mb-2"
          style={{ margin: '-16px' }}
        ></div>
        <form
          // ブラウザーの基本バリデーション非活性化
          noValidate
          // handleSubmitでバリデーション ➔ モーダル表示
          onSubmit={handleSubmit((input) => {
            onSubmit(input)
          })}
        >
          {/* フォームリスト */}
          <UserFormList register={register} errors={errors} gender={gender} />
          {/* 権限チェックボックス（登録画面のみ） */}
          <AuthBox register={register} />
          {/* フォームフッター */}
          <UserFormFooter
            reset={reset}
            setAuthorityCODE={setAuthorityCODE}
            navigate={navigate}
            navigatePath={'/manager'}
          />
        </form>
        {/* 確認モーダル */}
        <UserConfirmModal
          modalName="会員情報登録確認"
          // addUser: 登録関数
          addUser={addUser}
          show={show}
          handleClose={handleClose}
          id={userID}
          name={name}
          nameKana={nameKana}
          birthday={birthday}
          gender={gender}
          email={email}
          phone={phone}
          postCode={postCode}
          address={address}
          authorityCODE={authorityCODE}
          active={active}
          handleInactive={handleInactive}
        />
      </div>
    </div>
  )
}
