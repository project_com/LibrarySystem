import express from 'express'
import cors from 'cors'
import nodeMailer from 'nodemailer'

//import ほかのクラスのcomponent
import db from './db.config.js'
import QUERY from './db.query.js'
import logger from './logger.js'

const app = express()
app.use(express.urlencoded({ extended: true }))

app.use(cors())
app.use(express.json())

let idCheck //データベースにIDがあるかどうか確認
let idLogin //権限確認するためログインページで入力したIDを習得します。
let ID //メール送信するときIDを確認するため
let authorCode

//登録
app.post('/register', (req, res) => {
  const {
    id,
    password,
    name,
    nameKana,
    birthday,
    gender,
    email,
    phone,
    postCode,
    address,
    authorityCODE,
    regID,
  } = req.body
  db.query(
    QUERY.CREATE_USER,
    [
      id,
      name,
      nameKana,
      gender,
      birthday,
      email,
      phone,
      authorityCODE,
      password,
      postCode,
      address,
      regID,
    ],
    (err, result) => {
      if (err) {
        console.log(err)
        console.log('Values Not Inserted')
        if (err.sqlMessage.includes('PRIMARY')) {
          res.send({ message: 'dup id' })
        } else if (err.sqlMessage.includes('email_UNIQUE')) {
          res.send({ message: 'dup email' })
        } else if (err.sqlMessage.includes('phone_UNIQUE')) {
          res.send({ message: 'dup phone' })
        } else {
          res.send(err)
        }
      } else {
        res.send({ message: 'inserted' })
        console.log('Values Inserted')
      }
    }
  )
})

//ログイン
//安全性を維持するためPOSTを使う
app.post('/api/post', (req, res) => {
  const { id, password } = req.body
  logger.info(id, password) //Check value of id, password
  idLogin = id

  db.query(QUERY.SELECT_FOR_LOGIN, [id, password], (err, result) => {
    if (result.length > 0) {
      authorCode = result.map((r) => r.authority_CODE)[0]
      logger.info(`Logining ID is ${id}, auth_code is ${authorCode}`)
      res.send(result)
    } else {
      logger.warn('Wrong information!')
      res.send({ message: 'wrong!' })
    }
  })
})

//情報の確認
app.post('/api/check', (req, res) => {
  const { id, email } = req.body
  logger.info(`Email is ${email}, Id is ${id}`) //Check value of id, password
  idCheck = id

  db.query(QUERY.CHECK_INFO_PASS, [id, email], (err, result) => {
    if (result.length > 0) {
      logger.info(`Data corrrect for id: ${id}`)
      res.send(result)
    } else {
      logger.warn('Wrong Information')
      res.send({ message: 'wrong!' })
    }
  })
})

//パスワードの確認・変更
app.post('/api/checkpass', (req, res) => {
  const password = req.body.password
  logger.info(idLogin + ' 84')
  let idForPass
  if (idLogin == 0) {
    idForPass = idCheck
  } else {
    idForPass = idLogin
  }
  logger.info(`Password is ${password}, Id is ${idForPass}`)

  let checkPassId
  db.query(QUERY.CHECK_PASS, [password], (err1, result1) => {
    checkPassId = result1.map((r) => r.id)[0]
    let turn = checkPassId == idForPass
    logger.info(turn)

    if (!turn) {
      db.query(QUERY.UPDATE_PASSWORD, [password, idForPass], (err, result) => {
        logger.info(`Id ${idForPass} has changed password to ${password}. `)
        res.send({ message: 'change' })
      })
    } else {
      res.send({ message: 'error' })
    }
  })
})

//メール送信
app.post('/api/email', (req, res) => {
  const link = 'http://localhost:3000/'
  const { name, email } = req.body
  let nameCheck
  let turn
  db.query(QUERY.CHECK_INFO_SEND_MAIL, [email], (err, result) => {
    if (result) {
      ID = result.map((r) => r.id)[0]
      nameCheck = result.map((r) => r.name)[0]
      turn = name == nameCheck
    }

    nodeMailer.createTestAccount((err, account) => {
      const htmlEmail = `
            <h3>${name}様、 </h3>
            <p> ミリ図書館WEBサイトをご利用いただき、ありがとうございます。</p>
            <p> パスワード再発行お手続きを承りました。</p>
            <p> お客様のログインIDは <b> ${ID} </b> です。</p>
            <p> ログインページで再ログインしてください。</p>
            <a href="${link}">${link}</a>
        `
      let mailerConfig = {
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      }
      let transporter = nodeMailer.createTransport(mailerConfig)

      let mailOptions = {
        from: 'library@testemail.co',
        to: email,
        subject: 'パスワード再発行',
        html: htmlEmail,
      }

      if (ID && turn) {
        logger.info('142 -- Sent email to ID ' + ID)
        res.send({ message: 'Sent' })
        transporter.sendMail(mailOptions, (err, info) => {
          if (err) {
            res.status(500).send({
              status: 'FAIL',
              msg: 'Internal error: email not sent',
            })
          } else {
            res.status(200).json({
              status: 'OK',
              msg: 'Email sent',
            })
            logger.info('Message sent')
          }
        })
        return
      } else {
        logger.warn(`Name and email not correct!`)
        res.status(200).json({ status: 'error', msg: 'not send' })
      }
    })
  })
})

//管理者ページでデータの表示
app.get('/api/get', (req, res) => {
  db.query(QUERY.SELECT_USERS, (err, result) => {
    res.send(result)
    logger.info('Data has been loaded')
  })
})

//論理削除
app.delete('/api/remove/:id', (req, res) => {
  const { id } = req.params
  logger.info('ID is deleted is: ' + id)
  const sqlRemove = `DELETE FROM user WHERE id in(${id})`
  db.query(sqlRemove, (error, result) => {
    if (error) {
      res.send({ message: 'Same ID' })
      logger.error('Error occured' + error)
    }
  })
})

//物理削除
app.delete('/api/hide/:id', (req, res) => {
  const { id } = req.params
  logger.info('ID is hided is: ' + id)
  const sqlHide = `UPDATE user SET show_flag=1 WHERE id in (${id})`
  db.query(sqlHide, (error, result) => {
    if (error) {
      logger.error('Error occured' + error)
    }
  })
})

//修正
app.get('/get/:getMemberID', (req, res) => {
  const { getMemberID } = req.params
  db.query(QUERY.SELECT_USER, [getMemberID], (err, result) => {
    res.send(result)
  })
})

app.put('/update', (req, res) => {
  const {
    udpID,
    id,
    name,
    nameKana,
    birthday,
    gender,
    email,
    phone,
    postCode,
    address,
  } = req.body
  db.query(
    QUERY.UPDATE_USER,
    [
      name,
      nameKana,
      birthday,
      gender,
      email,
      phone,
      postCode,
      address,
      udpID,
      id,
    ],
    (err, result) => {
      if (err) {
        console.log(err)
        console.log('Values Not Updated')
        if (err.sqlMessage.includes('email_UNIQUE')) {
          res.send({ message: 'dup email' })
        } else if (err.sqlMessage.includes('phone_UNIQUE')) {
          res.send({ message: 'dup phone' })
        } else {
          res.send(err)
        }
      } else {
        console.log('Values Updated')
        res.send({ message: 'updated' })
      }
    }
  )
})

//ログインID取得
app.get('/getID', (req, res) => {
  res.send({ data: idLogin })
})

app.get('/getAuthCode', (req, res) => {
  res.send({ data: authorCode })
})

//Reset ID
app.get('/resetID', (req, res) => {
  idLogin = 0
})

app.put('/user/edit/update-personal', (req, res) => {
  const { id, email, phone, postCode, address } = req.body

  db.query(
    QUERY.UPDATE_PERSONAL_USER,
    // 'UPDATE user SET email = ?, phone = ?, postCode = ?, address = ?, upd_ID = ?, upd_date = now() WHERE id = ?',
    [email, phone, postCode, address, id, id],
    (err, result) => {
      if (err) {
        console.log(err)
        console.log('Values Not Updated')
        if (err.sqlMessage.includes('email_UNIQUE')) {
          res.send({ message: 'dup email' })
        } else if (err.sqlMessage.includes('phone_UNIQUE')) {
          res.send({ message: 'dup phone' })
        } else {
          res.send(err)
        }
      } else {
        console.log('Values Updated')

        res.send({ message: 'updated' })
      }
    }
  )
})

//Book
app.get('/books', (req, res) => {
  db.query(QUERY.SELECT_BOOKS, (err, result) => {
    res.send(result)
    logger.info('Data has been loaded')
  })
})

//Rental
app.get('/rental_detail', (req, res) => {
  db.query(QUERY.SELECT_RENTAL_DETAIL, [idLogin], (err, result) => {
    if (err) {
      logger.warn(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/period/checkPeriodFlag/:bookID', (req, res) => {
  const { bookID } = req.params
  logger.info('bookID is ' + bookID)
  db.query(QUERY.CHECK_BOOK_PERIOD, [bookID], (err, result) => {
    const flag = result.map((r) => r.ex_period_flag)[0]
    if (flag === 0) {
      db.query(QUERY.CHECK_RESERVE_WAITING, [bookID], (err, result2) => {
        if (result2.length !== 0) return res.send({ message: 'no' })
        res.send({ message: 'ok' })
        console.log('Can update')
      })
    } else {
      res.send({ message: 'no' })
    }
  })
})

app.put('/period/:bookID', (req, res) => {
  const { bookID } = req.params
  logger.info('bookID is ' + bookID)
  db.query(QUERY.UPDATE_PERIOD, [bookID], (err, result) => {
    if (result) {
      logger.info('Update period')
    } else {
      res.send(error)
      logger.error(error)
    }
  })
})

//ORDER
app.get('/order_detail', (req, res) => {
  db.query(QUERY.SELECT_ORDER_DETAIL, [idLogin], (err, result) => {
    if (err) {
      logger.warn(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/index-reserve/:bookID', (req, res) => {
  const userID = idLogin
  const { bookID } = req.params
  logger.info(userID)
  db.query(QUERY.SELECT_INDEX_RESERVE, [bookID, userID], (err, result) => {
    if (err) return logger.warn(err)
    logger.info(result)
    const index = result[1][0].indexes
    res.send({ data: index })
  })
})

app.delete('/cancel_order/:bookID', (req, res) => {
  const { bookID } = req.params
  logger.info('bookID is ' + bookID)
  db.query(QUERY.DELETE_ORDER, [bookID], (err, result) => {
    if (err) {
      logger.warn(err)
    } else {
      res.send(result)
    }
  })
})

//Manager reservation
app.get('/reservation-detail', (req, res) => {
  db.query(QUERY.SELECT_ALL_ORDER, (err, result) => {
    if (err) {
      logger.warn(err)
    } else {
      res.send(result)
    }
  })
})

app.put('/reserve-status-change/:bookID/:userID', (req, res) => {
  const { bookID, userID } = req.params
  logger.info(bookID)
  db.query(QUERY.CHECK_AVAIABLE_FLAG, [bookID], (err, result) => {
    if (!result) return logger.warn(err)
    const avaiableFlag = result[0].avaiable_flag
    logger.info(avaiableFlag === 0)
    if (avaiableFlag !== 0) return res.send({ message: 'rent-ing' })
    db.query(QUERY.CHANGE_STATUS_RESERVE, [bookID, userID], (err, result) => {
      if (err) {
        logger.warn(err)
      } else {
        res.send(result)
      }
    })
  })
})

//Manager rental
app.get('/rental-detail', (req, res) => {
  db.query(QUERY.SELECT_ALL_RENTAL, (err, result) => {
    if (err) {
      logger.warn(err)
    } else {
      res.send(result)
    }
  })
})

app.get('/book-detail-for-rental/:bookID', (req, res) => {
  const { bookID } = req.params
  db.query(QUERY.SELECT_BOOK_FOR_RENTAL, [bookID], (err, result) => {
    if (err) return
    logger.info(result)
    res.send(result)
  })
})

app.put('/register-rental-information', (req, res) => {
  const { userID, bookID, rentDate, returnDate } = req.body
  logger.info(userID, bookID, rentDate, returnDate)
  db.query(
    QUERY.INSERT_RENTAL,
    [userID, bookID, rentDate, returnDate, bookID],
    (err, result) => {
      if (err) return
      logger.info(result)
      res.send(result)
    }
  )
})

app.delete('/rental-delete/:bookID', (req, res) => {
  const { bookID } = req.params
  logger.info(bookID)
  db.query(QUERY.DELETE_RENTAL, [bookID], (err, result) => {
    if (err) {
      logger.warn(err)
    } else {
      res.send(result)
    }
  })
})

//コネクション確認
app.listen(3001, () => {
  logger.info('Listening from server 3001')
})
