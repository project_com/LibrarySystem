import axios from 'axios'
import { useEffect, useState } from 'react'
import { FaArrowsAltV } from 'react-icons/fa'
import { useNavigate } from 'react-router-dom'
import ToolkitProvider, {
  Search,
} from 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit'
import {
  BAR_BUTTON,
  CopyrightText,
  StyledFormButton,
} from '../../components/Style'
import BootstrapTable from 'react-bootstrap-table-next'
import paginationFactory from 'react-bootstrap-table2-paginator'
import cellEditFactory from 'react-bootstrap-table2-editor'

let id = []
function Books() {
  const navigate = useNavigate()

  const [data, setData] = useState([])
  const loadData = async () => {
    const response = await axios.get('http://localhost:3001/books')
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const selectRow = {
    mode: 'checkbox',
    clickToSelect: true,
    bgColor: '#aaa',
    onSelect: (row) => {
      console.log(row.book_ID)
      if (id.length === 0) {
        id.push(row.book_ID)
      } else {
        if (id.indexOf(row.book_ID) === -1) {
          id.push(row.book_ID)
        } else {
          id.splice(id.indexOf(row.book_ID), 1)
        }
      }
      console.log(id)
    },
  }

  const upDownFormatter = (column, colIndex) => {
    return (
      <span>
        <FaArrowsAltV />
        {column.text}
      </span>
    )
  }
  const regFormatter = (data, row) => {
    const d = data
    let day = new Date(d)
    return <span>{day.toLocaleDateString()}</span>
  }

  const codeFormatter = (data, row) => {
    const d = data
    switch (d) {
      case 0:
        return <span>在架</span>
      case 1:
        return <span>貸出中</span>
      case 2:
        return <span>予約中</span>
      default:
        return <span>貸出不可</span>
    }
  }

  const { SearchBar } = Search
  const columns = [
    {
      dataField: 'book_ID',
      text: 'ID',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'title',
      text: 'タイトル',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'author',
      text: '著者名',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'publisher',
      text: '出版社',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'pub_date',
      text: '出版日',
      formatter: regFormatter,
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'reg_ID',
      text: '登録者ID',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'reg_date',
      text: '登録日',
      formatter: regFormatter,
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'upd_ID',
      text: '更新者ID',
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'upd_date',
      text: '更新日',
      formatter: regFormatter,
      sort: true,
      headerFormatter: upDownFormatter,
    },
    {
      dataField: 'book_status_CODE',
      text: '書籍状況',
      formatter: codeFormatter,
      sort: true,
      headerFormatter: upDownFormatter,
    },
  ]

  return (
    <>
      <div className="justify-center">
        <h1
          className="mx-auto flex 
  flex col justify-center items-center"
          style={{
            textAlign: 'center',
            fontSize: '4.0em',
            padding: '50px',
          }}
        >
          書籍管理画面
        </h1>

        <BAR_BUTTON
          style={{ top: '2rem' }}
          onClick={() => navigate('/manager')}
        >
          Back
        </BAR_BUTTON>

        <div
          className="max-w-5xl flex flex col justify-center items-center"
          style={{ margin: '15px 75px' }}
        >
          <StyledFormButton onClick={() => navigate('/register')}>
            新規登録
          </StyledFormButton>
          {/* <StyledFormButton onClick={() => deleteMember(id)}> */}
          {/* <StyledFormButton>削除</StyledFormButton> */}
          {/* <StyledFormButton onClick={() => hideMember(id)}> */}
          {/* <StyledFormButton>隠し</StyledFormButton> */}
          {/* <StyledFormButton
          // onClick={() => {
          //   toEditPage()
          // }}
          >
            修正
          </StyledFormButton> */}
        </div>

        <div
          style={{
            maxWidth: '1500px',
          }}
        >
          <ToolkitProvider
            keyField="book_ID"
            data={data}
            columns={columns}
            search
          >
            {(props) => (
              <div>
                <SearchBar
                  placeholder="ID、タイトル…"
                  srText="書籍情報検索"
                  delay="700"
                  {...props.searchProps}
                />

                <hr />
                <BootstrapTable
                  {...props.baseProps}
                  striped
                  hover
                  condensed
                  pagination={paginationFactory()}
                  cellEdit={cellEditFactory({})}
                  selectRow={selectRow}
                />
              </div>
            )}
          </ToolkitProvider>
          <CopyrightText>All rights reserved &copy;2022</CopyrightText>
        </div>
      </div>
    </>
  )
}

export default Books
