//データベースのクエリ
//User
const QUERY = {
  CREATE_USER:
    'INSERT INTO user (id, name, name_kana, gender, birthday, email, phone, authority_CODE, password, postCode, address, reg_ID, reg_date) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,now())',
  SELECT_USERS:
    'SELECT id, name, name_kana, gender, birthday, email, phone, reg_ID, reg_date FROM user WHERE show_flag=0',
  SELECT_USER: 'SELECT * FROM user WHERE id=?',
  UPDATE_USER:
    'UPDATE user SET name = ?, name_kana = ?, birthday = ?, gender = ?, email = ?, phone = ?, postCode = ?, address = ?, upd_ID = ?, upd_date = now() WHERE id = ?',
  CHECK_INFO_SEND_MAIL:
    'SELECT id, name FROM user WHERE email= ? and show_flag=0',
  SELECT_FOR_LOGIN:
    'SELECT id, password, authority_CODE FROM user WHERE id=? and password=?',
  CHECK_INFO_PASS:
    'SELECT id, email FROM user WHERE id=? and email=? and show_flag=0',
  CHECK_PASS: 'SELECT id FROM user WHERE password=?',
  UPDATE_PASSWORD: 'UPDATE user SET password=? WHERE id=?',
  // DELETE_USER: 'DELETE FROM user WHERE id in(?)',
  // HIDE_USER: 'UPDATE user SET show_flag=1 WHERE id in (?)',
  UPDATE_PERSONAL_USER:
    'UPDATE user SET email = ?, phone = ?, postCode = ?, address = ?, upd_ID = ?, upd_date = now() WHERE id = ?',

  //Book
  SELECT_BOOKS: 'SELECT * FROM book',
  //Rental query
  SELECT_RENTAL_DETAIL:
    // 'SELECT b.book_ID, b.title, b.author, b.publisher, r.rent_date, r.return_date, r.ex_period_flag FROM book b JOIN rental r ON b.book_ID = r.book_ID WHERE member_ID=?',
    'SELECT b.book_ID, b.title, b.author, b.publisher, r.rent_date, r.return_date, r.ex_period_flag FROM book b JOIN rental r ON b.book_ID = r.book_ID WHERE user_ID=?',

  UPDATE_PERIOD:
    'update rental set return_date = (select date_add(return_date, INTERVAL 15 DAY)), ex_period_flag = 1 where ex_period_flag = 0 and book_ID=?',
  CHECK_BOOK_PERIOD: 'select ex_period_flag from rental where book_ID=?',
  CHECK_RESERVE_WAITING: 'SELECT * FROM reservation WHERE book_ID = ?',
  //Reserve query
  SELECT_ORDER_DETAIL:
    'select b.book_ID, b.title, b.author, b.publisher, o.reserved_date, o.status FROM book b JOIN reservation o ON b.book_ID = o.book_ID WHERE user_ID=? order by o.book_ID',
  // `set @row_num = 0; select b.book_ID, b.title, b.author, b.publisher, o.reserved_date, o.status, f.indexes FROM book b JOIN reservation o ON b.book_ID = o.book_ID JOIN (select indexes,user_ID,book_ID from (select (@row_num := @row_num + 1) as indexes, book_ID, user_ID from reservation where book_ID = ? order BY book_ID) a where user_ID = ?) f on b.book_ID = f.book_ID WHERE o.user_ID=? order by o.book_ID;`,
  SELECT_INDEX_RESERVE:
    'set @row_num = 0; select indexes from (select (@row_num := @row_num + 1) as indexes, book_ID, user_ID from reservation where book_ID = ? order BY book_ID) a where user_ID = ?;',

  DELETE_ORDER: 'DELETE FROM reservation WHERE book_ID=?',

  //Manager Reserve Query
  SELECT_ALL_ORDER:
    'SELECT r.user_ID, r.book_ID, b.title, b.author, b.publisher, r.reserved_date, r.status from reservation r join book b on r.book_ID = b.book_ID',
  CHECK_AVAIABLE_FLAG: 'SELECT avaiable_flag from book where book_ID=?',
  CHANGE_STATUS_RESERVE:
    'UPDATE reservation set status = 1 WHERE book_ID=? and user_ID=?',

  //Manager Rental Query
  SELECT_ALL_RENTAL:
    // 'SELECT r.member_ID, r.book_ID, b.title, b.author, b.publisher, r.rent_date, r.return_date, r.rental_status_CODE from rental r join book b on r.book_ID = b.book_ID',
    'SELECT r.user_ID, r.book_ID, b.title, b.author, b.publisher, r.rent_date, r.return_date, r.ex_period_flag from rental r join book b on r.book_ID = b.book_ID',
  SELECT_BOOK_FOR_RENTAL:
    'SELECT book_ID, title, author, publisher from book where book_ID=? ',
  INSERT_RENTAL:
    'INSERT INTO rental (user_ID, book_ID, rent_date, return_date) VALUES (?,?,?,?); UPDATE book set rental_flag =1 WHERE book_ID=?;',
  // UPDATE_BOOK_FLAG: 'UPDATE book set rental_flag =1 WHERE book_ID=?',
  DELETE_RENTAL: 'DELETE FROM rental WHERE book_ID=?',
}

export default QUERY
