import { Outlet, Navigate } from 'react-router-dom'

const ProtectedManagerRoutes = ({ isManager }) => {
  return isManager ? <Outlet /> : <Navigate to="/" />
}
export default ProtectedManagerRoutes
