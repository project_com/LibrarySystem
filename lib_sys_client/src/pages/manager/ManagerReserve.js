import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
} from '@mui/material'
import axios from 'axios'
import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import {
  BACK_BUTTON,
  BLANK_PAGE_TITLE,
  RENTAL_BUTTON,
  RENTAL_TITLE,
} from '../../components/Style'

function ManagerReserve() {
  const [data, setData] = useState([])
  const navigate = useNavigate()

  const loadData = async () => {
    const response = await axios.get('http://localhost:3001/reservation-detail')
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const handleStatus = (row) => {
    if (row === 0) {
      return '予約中'
    } else {
      return '用意できる'
    }
  }

  const handleCheck = (row) => {
    const bookID = row.book_ID
    const userID = row.user_ID
    let r = window.confirm('予約リクエスト完了よろしいでしょうか。')
    if (!r) return
    axios
      .put(`http://localhost:3001/reserve-status-change/${bookID}/${userID}`)
      .then((response) => {
        if (response.data.message) return window.alert('書籍は貸出中です。')
        window.alert('完了しました。')
        setTimeout(() => {
          loadData()
        }, 300)
      })
  }
  if (data.length === 0) {
    return (
      <div>
        <BLANK_PAGE_TITLE>会員予約情報がありません</BLANK_PAGE_TITLE>

        <BACK_BUTTON onClick={() => navigate('/manager')}>戻る</BACK_BUTTON>
      </div>
    )
  } else {
    return (
      <>
        <div style={{ border: 'solid 1px #9ED2C6' }}>
          <RENTAL_TITLE>会員予約状況</RENTAL_TITLE>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    ユーザーID
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    書籍ID
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    タイトル
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    著者名
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    出版社名
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    予約日
                  </TableCell>
                  <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                    状況
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {data.map((row) => (
                  <TableRow
                    key={row.index}
                    sx={{ '&:lastChild td, &:lastChild th': { border: 0 } }}
                    selected
                  >
                    <TableCell>{row.user_ID}</TableCell>
                    <TableCell>{row.book_ID}</TableCell>
                    <TableCell>{row.title}</TableCell>
                    <TableCell>{row.author}</TableCell>
                    <TableCell>{row.publisher}</TableCell>
                    <TableCell style={{ paddingLeft: '1.5rem' }}>
                      {new Date(row.reserved_date).toLocaleDateString()}
                    </TableCell>
                    <TableCell>{handleStatus(row.status)}</TableCell>
                    {row.status === 0 ? (
                      <TableCell>
                        <RENTAL_BUTTON onClick={() => handleCheck(row)}>
                          確定
                        </RENTAL_BUTTON>
                      </TableCell>
                    ) : (
                      <TableCell>取り引き待ち</TableCell>
                    )}
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <BACK_BUTTON
            style={{ margin: '2rem 25rem' }}
            onClick={() => navigate('/manager')}
          >
            戻る
          </BACK_BUTTON>
        </div>
      </>
    )
  }
}

export default ManagerReserve
