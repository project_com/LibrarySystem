import Form from 'react-bootstrap/Form'

// 性別選択ラジオボタン
export default function GenderPicker({ gender, register }) {
  return (
    <Form.Group className="mt-2">
      <Form.Label className="fw-bold">性別</Form.Label>
      <div>
        <div className="form-check form-check-inline">
          <input
            type="radio"
            id="inlineRadio1"
            name="inlineRadioOptions"
            className="form-check-input"
            value="m"
            {...register('gender', { required: true })}
            defaultChecked={gender === 'm'}
          />
          <label className="form-check-label" htmlFor="inlineRadio1">
            男性
          </label>
        </div>
        <div className="form-check form-check-inline">
          <input
            type="radio"
            id="inlineRadio2"
            name="inlineRadioOptions"
            className="form-check-input"
            value="f"
            {...register('gender', { required: true })}
            defaultChecked={gender === 'f'}
          />
          <label className="form-check-label" htmlFor="inlineRadio2">
            女性
          </label>
        </div>
      </div>
    </Form.Group>
  )
}
