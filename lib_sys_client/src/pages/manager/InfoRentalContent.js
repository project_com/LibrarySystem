import React, { useRef } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTriangleExclamation } from '@fortawesome/free-solid-svg-icons'
import { CANCEL_BUTTON, LINE, TABLE, TABLE_TD } from '../../components/Style'
import axios from 'axios'

export default function InfoRentalContent({ data, open }) {
  const inputValue = useRef(null)
  const toDay = new Date()
  const plusDays = new Date()
  plusDays.setDate(plusDays.getDate() + 15)

  const register = (row) => {
    const bookID = row.book_ID
    const userID = inputValue.current.value
    const rentDate = toDay.toLocaleDateString()
    const returnDate = plusDays.toLocaleDateString()

    console.log(bookID, userID, rentDate, returnDate)

    let r = window.confirm('レンタル情報を登録よろしいですか。')
    if (!r) return
    axios.put('http://localhost:3001/register-rental-information', {
      bookID,
      userID,
      rentDate,
      returnDate,
    })

    window.alert('レンタル情報を登録しました。')
  }

  if (!open) return null
  return (
    <>
      {data.length !== 0 ? (
        <div>
          <h3 style={{ marginBottom: '2rem' }}>
            <FontAwesomeIcon icon={faTriangleExclamation} /> レンタル情報
          </h3>
          <LINE />
          {data.map((row) => (
            <div>
              <TABLE>
                <tbody key={row.index}>
                  <tr>
                    <TABLE_TD>書籍ID</TABLE_TD>
                    <TABLE_TD>{row.book_ID}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>タイトル</TABLE_TD>
                    <TABLE_TD>{row.title}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>著者名</TABLE_TD>
                    <TABLE_TD>{row.author}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>出版社</TABLE_TD>
                    <TABLE_TD>{row.publisher}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>貸出日</TABLE_TD>
                    <TABLE_TD>{toDay.toLocaleString() + ''}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>返却日</TABLE_TD>
                    <TABLE_TD>{plusDays.toLocaleString() + ''}</TABLE_TD>
                  </tr>
                  <tr>
                    <TABLE_TD>ユーザーID</TABLE_TD>
                    <TABLE_TD>
                      <input ref={inputValue} type="search"></input>
                    </TABLE_TD>
                  </tr>
                </tbody>
              </TABLE>
              <CANCEL_BUTTON onClick={() => register(row)}>登録</CANCEL_BUTTON>
            </div>
          ))}
        </div>
      ) : (
        <h3>書籍情報が致しません。</h3>
      )}
    </>
  )
}
