import React, { useEffect, useState } from 'react'
import {
  TableContainer,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Paper,
} from '@mui/material'
import axios from 'axios'
import {
  BACK_BUTTON,
  BLANK_PAGE_TITLE,
  RENTAL_BUTTON,
  RENTAL_TITLE,
} from '../../components/Style'
import { useNavigate } from 'react-router-dom'

export default function Rental() {
  const navigate = useNavigate()
  const [data, setData] = useState([])

  const loadData = async () => {
    const response = await axios.get('http://localhost:3001/rental_detail')
    setData(response.data)
  }

  useEffect(() => {
    loadData()
  }, [])

  const handlePeriod = async (row) => {
    const bookID = row.book_ID
    const result = await axios.get(
      `http://localhost:3001/period/checkPeriodFlag/${bookID}`
    )
    switch (result.data.message) {
      case 'ok':
        if (
          window.confirm(
            '貸出資料は、貸出期間内であれば、１回だけ、延長の手続きをした日から１５日間の延長ができます。延長しますか。'
          )
        ) {
          console.log(bookID)
          axios.put(`http://localhost:3001/period/${bookID}`)
          window.alert('完了しました。')
          setTimeout(() => {
            loadData()
          }, 500)
        }
        break
      case 'no':
        window.alert('予約している人がいますので延長できません。')
        break
      default:
        break
    }
  }

  if (data.length === 0) {
    return (
      <div>
        <BLANK_PAGE_TITLE>貸出情報がありません</BLANK_PAGE_TITLE>

        <BACK_BUTTON onClick={() => navigate('/userview')}>戻る</BACK_BUTTON>
      </div>
    )
  } else {
    return (
      <div style={{ border: 'solid 1px #9ED2C6' }}>
        <RENTAL_TITLE>貸出状況</RENTAL_TITLE>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                  書籍ID
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                  タイトル
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                  著者名
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                  出版社名
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                  貸出日
                </TableCell>
                <TableCell style={{ color: '#2B4865', fontSize: '25px' }}>
                  返却日
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow
                  key={row.book_ID}
                  sx={{ '&:lastChild td, &:lastChild th': { border: 0 } }}
                  selected
                >
                  <TableCell>{row.book_ID}</TableCell>
                  <TableCell>{row.title}</TableCell>
                  <TableCell>{row.author}</TableCell>
                  <TableCell>{row.publisher}</TableCell>
                  <TableCell>
                    {new Date(row.rent_date).toLocaleDateString()}
                  </TableCell>
                  <TableCell>
                    {new Date(row.return_date).toLocaleDateString()}
                  </TableCell>
                  <TableCell>
                    {row.ex_period_flag === 0 ? (
                      <RENTAL_BUTTON onClick={() => handlePeriod(row)}>
                        延長
                      </RENTAL_BUTTON>
                    ) : (
                      <p
                        style={{
                          textAlign: 'center',
                          paddingTop: '0.7rem',
                        }}
                      >
                        延長しました
                      </p>
                    )}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <BACK_BUTTON
          style={{ margin: '2rem 25rem' }}
          onClick={() => navigate('/userview')}
        >
          戻る
        </BACK_BUTTON>
      </div>
    )
  }
}
