import { WRAPPER } from '../Style.js'

export const MENU_ITEMS = [
  {
    title: 'ユーザー画面へ',
    to: '/userview',
  },
  {
    title: '書籍画面へ',
    to: '/book',
  },
  {
    title: '予約管理画面へ',
    to: '/manager-reserve',
  },
  {
    title: '貸出管理画面へ',
    to: '/manager-rental',
  },
  {
    title: 'ログアウト',
    to: '/',
  },
]

export function Wrapper({ children, className }) {
  return <WRAPPER className={className}>{children}</WRAPPER>
}
