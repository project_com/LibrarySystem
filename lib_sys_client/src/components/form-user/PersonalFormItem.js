import Form from 'react-bootstrap/Form'

// 個人情報修正フォームの項目
export default function PersonalFormItem({ itemName, defaultValue }) {
  return (
    <Form.Group className="mt-2">
      <Form.Label className="fw-bold">{itemName}</Form.Label>
      <p style={{ fontSize: '15px' }}>{defaultValue}</p>
    </Form.Group>
  )
}
