import DatePicker from './DatePicker'
import GenderPicker from './GenderPicker'
import UserFormItem from './UserFormItem'

// ユーザーフォームのリスト
export default function UserFormList({
  register,
  errors,
  name,
  nameKana,
  birthday,
  gender,
  email,
  phone,
  postCode,
  address,
}) {
  return (
    <>
      <UserFormItem
        itemName="名前（漢字）"
        defaultValue={name}
        register={register}
        registerName="name"
        type="text"
        placeholder="図書タロウ"
        error={errors?.name?.message}
      />
      <UserFormItem
        itemName="名前（カタカナ）"
        defaultValue={nameKana}
        register={register}
        registerName="nameKana"
        type="text"
        placeholder="トショタロウ"
        error={errors?.nameKana?.message}
      />
      {/* 生年月日入力ボックス */}
      <DatePicker register={register} errors={errors} birthday={birthday} />
      {/* 性別選択ラジオボタン */}
      <GenderPicker register={register} gender={gender} />
      <UserFormItem
        itemName="メールアドレス"
        defaultValue={email}
        register={register}
        registerName="email"
        type="email"
        placeholder="mirine@global.com"
        error={errors?.email?.message}
      />
      <UserFormItem
        itemName="電話番号"
        defaultValue={phone}
        register={register}
        registerName="phone"
        type="number"
        placeholder="09012345678"
        error={errors?.phone?.message}
      />
      <UserFormItem
        itemName="郵便番号"
        defaultValue={postCode}
        register={register}
        registerName="postCode"
        type="number"
        placeholder="1234567"
        error={errors?.postCode?.message}
      />
      <UserFormItem
        itemName="住所"
        defaultValue={address}
        register={register}
        registerName="address"
        type="text"
        placeholder="東京都豊島区駒込１ー２ー３ミリネビル２０１"
        error={errors?.address?.message}
      />
    </>
  )
}
