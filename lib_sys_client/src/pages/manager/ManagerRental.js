import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import cellEditFactory from "react-bootstrap-table2-editor";
import ToolkitProvider, {
    Search,
} from "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit";

import {
    BACK_BUTTON,
    BLANK_PAGE_TITLE,
    CopyrightText,
    StyledFormButton,
} from "../../components/Style";
import BookFind from "./BookFind";

function ManagerRental() {
    let id = [];

    const [data, setData] = useState([]);
    const [isOpen, setIsOpen] = useState(false);

    const navigate = useNavigate();

    const loadData = async () => {
        const response = await axios.get("http://localhost:3001/rental-detail");
        setData(response.data);
    };

    useEffect(() => {
        loadData();
    }, []);

    const selectRow = {
        mode: "checkbox",
        clickToSelect: true,
        bgColor: "#aaa",
        onSelect: (row) => {
            console.log(row.id);
            if (id.length === 0) {
                id.push(row.book_ID);
            } else {
                if (id.indexOf(row.book_ID) === -1) {
                    id.push(row.book_ID);
                } else {
                    id.splice(id.indexOf(row.book_ID), 1);
                }
            }
            console.log(id);
        },
    };

    const handleStatus = (data, row) => {
        const d = data;
        if (d === 0) {
            return "貸出中";
        } else {
            return "延長中";
        }
    };

    const dateFormatter = (data, row) => {
        const d = data;
        let day = new Date(d);
        return <span>{day.toLocaleDateString()}</span>;
    };

    const handleDelete = (id) => {
        if (id.length !== 1) {
            window.alert("1つだけ選択してください。");
        } else {
            let r = window.confirm("貸出情報の削除よろしいでしょうか。");
            if (r) {
                axios.delete(`http://localhost:3001/rental-delete/${id}`);
                window.alert("削除が完了しました。");
                setTimeout(() => {
                    loadData();
                }, 500);
            }
        }
    };

    const { SearchBar } = Search;
    const columns = [
        {
            // dataField: 'member_ID',
            dataField: "user_ID",
            text: "ユーザーID",
        },
        {
            dataField: "book_ID",
            text: "書籍ID",
        },
        {
            dataField: "title",
            text: "タイトル",
        },
        {
            dataField: "author",
            text: "著者名",
        },
        {
            dataField: "publisher",
            text: "出版社名",
        },
        {
            dataField: "rent_date",
            text: "貸出日",
            formatter: dateFormatter,
        },
        {
            dataField: "return_date",
            text: "返却日",
            formatter: dateFormatter,
        },
        {
            dataField: "ex_period_flag",
            text: "状況",
            formatter: handleStatus,
        },
    ];
    if (data.length === 0) {
        return (
            <div>
                <BLANK_PAGE_TITLE>会員貸出情報がありません</BLANK_PAGE_TITLE>

                <BACK_BUTTON onClick={() => navigate("/manager")}>
                    戻る
                </BACK_BUTTON>
            </div>
        );
    } else {
        return (
            <div className="justify-center">
                <h1
                    className="mx-auto flex 
            flex col justify-center items-center"
                    style={{
                        textAlign: "center",
                        fontSize: "4.0em",
                        padding: "50px",
                    }}
                >
                    会員レンタル情報管理画面
                </h1>

                <div
                    className="max-w-5xl flex flex col justify-center items-center"
                    style={{ margin: "15px 15rem" }}
                >
                    <StyledFormButton onClick={() => navigate("/manager")}>
                        戻る
                    </StyledFormButton>
                    <StyledFormButton onClick={() => setIsOpen(true)}>
                        レンタル登録
                    </StyledFormButton>
                    <StyledFormButton onClick={() => handleDelete(id)}>
                        削除
                    </StyledFormButton>
                </div>
                {isOpen && (
                    <BookFind show={isOpen} close={() => setIsOpen(false)} />
                )}

                <div
                    style={{
                        maxWidth: "1000px",
                    }}
                >
                    <ToolkitProvider
                        keyField="book_ID"
                        data={data}
                        columns={columns}
                        search
                    >
                        {(props) => (
                            <div>
                                <SearchBar
                                    placeholder="ユーザーID、書籍ID、タイトル…"
                                    srText="会員貸出情報検索"
                                    delay="700"
                                    {...props.searchProps}
                                />
                                <hr />
                                <BootstrapTable
                                    {...props.baseProps}
                                    striped
                                    hover
                                    condensed
                                    pagination={paginationFactory()}
                                    cellEdit={cellEditFactory({})}
                                    selectRow={selectRow}
                                />
                            </div>
                        )}
                    </ToolkitProvider>
                    <CopyrightText>
                        All rights reserved &copy;2022
                    </CopyrightText>
                </div>
            </div>
        );
    }
}

export default ManagerRental;
