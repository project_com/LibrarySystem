import Form from 'react-bootstrap/Form'
import { StyledErrorMsg } from '../../components/Style'

// 生年月日選択ボックス
export default function DatePicker({ register, errors, birthday }) {
  return (
    <Form.Group className="mt-2">
      <Form.Label className="fw-bold">生年月日</Form.Label>
      <div className="col-md-auto">
        <Form.Group>
          <Form.Control
            defaultValue={birthday}
            type="date"
            name="dob"
            {...register('birthday')}
          />
          <StyledErrorMsg className="errors">
            {errors?.birthday?.message}
          </StyledErrorMsg>
        </Form.Group>
      </div>
    </Form.Group>
  )
}

// 入力した生年月日データフォーマッター　例）1999-05-17
export const formatInputBirthday = (date) => {
  let d = new Date(date)
  let month = (d.getMonth() + 1).toString()
  let day = d.getDate().toString()
  let year = d.getFullYear()
  if (month.length < 2) {
    month = '0' + month
  }
  if (day.length < 2) {
    day = '0' + day
  }
  return [year, month, day].join('-')
}

// 出力する生年月日データフォーマッター　例）1999-05-17
export const formatOutputBirthday = (date) => {
  let d = new Date(date)
  return (
    d.getFullYear() +
    '-' +
    (d.getMonth() + 1 > 9
      ? (d.getMonth() + 1).toString()
      : '0' + (d.getMonth() + 1)) +
    '-' +
    (d.getDate() > 9 ? d.getDate().toString() : '0' + d.getDate().toString())
  )
}

// 例）1999/05/17
export const formatDate = (date) => {
  let d = new Date(date)
  let month = (d.getMonth() + 1).toString()
  let day = d.getDate().toString()
  let year = d.getFullYear()
  if (month.length < 2) {
    month = '0' + month
  }
  if (day.length < 2) {
    day = '0' + day
  }
  return [year, month, day].join('/')
}
