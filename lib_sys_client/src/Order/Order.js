import {
    Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
} from "@mui/material";
import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import {
    BACK_BUTTON,
    BLANK_PAGE_TITLE,
    RENTAL_BUTTON,
    RENTAL_TITLE,
} from "../components/Style";
import Cancel from "./Cancel";

function Order() {
    const [data, setData] = useState([]);
    const [title, setTitle] = useState();
    const [author, setAuthor] = useState();
    const [publisher, setPublisher] = useState();
    const [bookID, setBookID] = useState();

    const navigate = useNavigate();

    const loadData = async () => {
        const response = await axios.get("http://localhost:3001/order_detail");
        setData(response.data);
    };

    useEffect(() => {
        loadData();
    }, []);

    const [isOpen, setIsOpen] = useState(false);

    const current = new Date();

    const handleStatus = (row) => {
        if (row === 0) {
            return "予約中";
        } else {
            current.setDate(current.getDate() + 7);
            return (
                <div>
                    用意できる。
                    <br />
                    {current.toLocaleDateString()} までに取引してください。
                </div>
            );
        }
    };

    const handleIndexOfReserve = (row) => {
        const bookID = row.book_ID;
        axios
            .get(`http://localhost:3001/index-reserve/${bookID}`)
            .then((response) => {
                console.log(response.data.data);
                window.alert("予約順位は： " + response.data.data);
            });
    };

    const handleCancel = (bookID) => {
        console.log(bookID);
        axios.delete(`http://localhost:3001/cancel_order/${bookID}`);
        window.alert("キャンセルが完了しました。");
        setTimeout(() => {
            setIsOpen(false);
            loadData();
        }, 300);
    };

    if (data.length === 0) {
        return (
            <div>
                <BLANK_PAGE_TITLE>予約情報がありません</BLANK_PAGE_TITLE>

                <BACK_BUTTON onClick={() => navigate("/userview")}>
                    戻る
                </BACK_BUTTON>
            </div>
        );
    } else {
        return (
            <>
                <div style={{ border: "solid 1px #9ED2C6" }}>
                    <RENTAL_TITLE>予約状況</RENTAL_TITLE>
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell
                                        style={{
                                            color: "#2B4865",
                                            fontSize: "25px",
                                        }}
                                    >
                                        書籍ID
                                    </TableCell>
                                    <TableCell
                                        style={{
                                            color: "#2B4865",
                                            fontSize: "25px",
                                        }}
                                    >
                                        タイトル
                                    </TableCell>
                                    <TableCell
                                        style={{
                                            color: "#2B4865",
                                            fontSize: "25px",
                                        }}
                                    >
                                        著者名
                                    </TableCell>
                                    <TableCell
                                        style={{
                                            color: "#2B4865",
                                            fontSize: "25px",
                                        }}
                                    >
                                        出版社名
                                    </TableCell>
                                    <TableCell
                                        style={{
                                            color: "#2B4865",
                                            fontSize: "25px",
                                        }}
                                    >
                                        予約日
                                    </TableCell>
                                    <TableCell
                                        style={{
                                            color: "#2B4865",
                                            fontSize: "25px",
                                        }}
                                    >
                                        状況
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.map((row) => (
                                    <TableRow
                                        key={row.book_ID}
                                        sx={{
                                            "&:lastChild td, &:lastChild th": {
                                                border: 0,
                                            },
                                        }}
                                        selected
                                    >
                                        <TableCell>{row.book_ID}</TableCell>
                                        <TableCell>{row.title}</TableCell>
                                        <TableCell>{row.author}</TableCell>
                                        <TableCell>{row.publisher}</TableCell>
                                        <TableCell>
                                            {new Date(
                                                row.reserved_date
                                            ).toLocaleDateString()}
                                        </TableCell>
                                        <TableCell>
                                            {handleStatus(row.status)}
                                        </TableCell>
                                        <TableCell>
                                            <RENTAL_BUTTON
                                                onClick={() => {
                                                    setIsOpen(true);
                                                    setTitle(row.title);
                                                    setAuthor(row.author);
                                                    setPublisher(row.publisher);
                                                    setBookID(row.book_ID);
                                                }}
                                            >
                                                キャンセル
                                            </RENTAL_BUTTON>
                                        </TableCell>
                                        <TableCell>
                                            <RENTAL_BUTTON
                                                onClick={() =>
                                                    handleIndexOfReserve(row)
                                                }
                                            >
                                                予約順番
                                            </RENTAL_BUTTON>
                                        </TableCell>
                                        <Cancel
                                            show={isOpen}
                                            close={() => setIsOpen(false)}
                                            title={title}
                                            author={author}
                                            publisher={publisher}
                                            cancel={() => handleCancel(bookID)}
                                        />
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <BACK_BUTTON
                        style={{ margin: "2rem 30rem" }}
                        onClick={() => navigate("/userview")}
                    >
                        戻る
                    </BACK_BUTTON>
                </div>
            </>
        );
    }
}

export default Order;
