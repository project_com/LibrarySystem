import React from 'react'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

// 登録・修正確認モーダル
export default function UserConfirmModal({
  show,
  handleClose,
  addUser,
  updUser,
  id,
  name,
  nameKana,
  birthday,
  gender,
  email,
  phone,
  postCode,
  address,
  authorityCODE,
  modalName,
  active,
  handleInactive,
}) {
  const items = {
    ユーザーID: id,
    '名前(漢字)': name,
    '名前(カタカナ)': nameKana,
    // 1999-06-15 ➔ 1999/06/15
    生年月日: birthday.replace(/-/gi, '/'),
    性別: gender === 'm' ? '男性' : '女性',
    メールアドレス: email,
    // 09048621681 ➔ 090-4862-1681
    電話番号:
      phone.substr(0, 3) + '-' + phone.substr(3, 4) + '-' + phone.substr(7, 4),
    // 1234567 ➔ 123-4567
    郵便番号: postCode.substr(0, 3) + '-' + postCode.substr(3, 4),
    住所: address,
    権限: authorityCODE === 1 ? '管理者' : '一般会員',
  }
  //モーダルの内容を表示
  const itemList = (items) => {
    // itemsオブジェクトのキー配列
    const itemsKeys = Object.keys(items)
    // itemsオブジェクトのバリュー配列
    const itemsValues = Object.values(items)
    // リストを格納するための空配列を生成
    const result = []
    // for文を回してHTMLコード生成、result配列に追加
    for (let i = 0; i < itemsKeys.length; i++) {
      result.push(
        <Row key={i} className="mb-2">
          <Col sm={4} className="text-end text-secondary">
            {itemsKeys[i]}
          </Col>
          <Col sm={6}>{itemsValues[i]} </Col>
        </Row>
      )
    }
    // result配列をリターン
    return result
  }
  return (
    <Modal
      // モーダルのプロパティ
      show={show}
      onHide={handleClose}
      backdrop="static"
      keyboard={false}
    >
      {/* モーダルを閉じるボタン */}
      <Modal.Header closeButton>
        <Modal.Title>{modalName}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {/* モーダルの中身 */}
        {itemList(items)}
      </Modal.Body>
      <Modal.Footer>
        {/* 戻るボタン。押下時モーダルを閉じる。 */}
        <Button variant="outline-secondary" onClick={handleClose}>
          戻る
        </Button>
        {/* 確定ボタン */}
        <Button
          disabled={active}
          variant="primary"
          onClick={() => {
            // ボタン非活性化
            handleInactive()
            // 登録画面からだとaddUser関数呼び出し
            addUser && addUser()
            // 修正画面からだとupdUser
            updUser && updUser()
          }}
        >
          確定
        </Button>
      </Modal.Footer>
    </Modal>
  )
}
