import Form from 'react-bootstrap/Form'
import { StyledErrorMsg } from '../../components/Style'

// ユーザーフォームの項目
export default function UserFormItem({
  itemName,
  type,
  placeholder,
  register,
  registerName,
  error,
  defaultValue,
}) {
  return (
    <Form.Group className="mt-2">
      <Form.Label className="fw-bold">{itemName}</Form.Label>
      <Form.Control
        type={type}
        placeholder={placeholder}
        defaultValue={defaultValue}
        {...register(registerName)}
      />
      <StyledErrorMsg>{error}</StyledErrorMsg>
    </Form.Group>
  )
}
