import Button from 'react-bootstrap/Button'

export default function UserFormFooter({
  navigate,
  navigatePath,
  reset,
  getUser,
}) {
  return (
    <>
      <div className="border-bottom mt-2" style={{ margin: '-16px' }}></div>
      <div className="float-start" style={{ marginTop: '30px' }}>
        {/* 戻るボタン */}
        <Button
          variant="outline-secondary"
          onClick={() => {
            // 管理者画面へ遷移
            navigate(navigatePath)
          }}
        >
          戻る
        </Button>
      </div>
      <div className="float-end" style={{ marginTop: '30px' }}>
        {/* クリアボタン */}
        <Button
          variant="danger"
          onClick={() => {
            // 登録画面フォームクリア
            reset && reset()
            // 修正画面フォームリセット
            getUser && getUser()
          }}
        >
          リセット
        </Button>
        {/* 確認ボタン、submit */}
        <Button type="submit" className="ms-2" variant="primary">
          確認
        </Button>
      </div>
    </>
  )
}
