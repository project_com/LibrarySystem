import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

import RouteRoot from './routes/RouteRoot'

function App() {
  return (
    <Router>
      <RouteRoot />
    </Router>
  )
}
export default App
