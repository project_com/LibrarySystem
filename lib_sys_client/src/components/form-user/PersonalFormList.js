import PersonalFormItem from './PersonalFormItem'
import UserFormItem from './UserFormItem'

// 個人情報修正フォームのリスト
export default function PersonalFormList({
  register,
  errors,
  name,
  nameKana,
  birthday,
  gender,
  email,
  phone,
  postCode,
  address,
}) {
  return (
    <>
      <PersonalFormItem itemName="名前（漢字）" defaultValue={name} />
      <PersonalFormItem itemName="名前（カタカナ）" defaultValue={nameKana} />
      <PersonalFormItem itemName="生年月日" defaultValue={birthday} />
      <PersonalFormItem
        itemName="性別"
        defaultValue={gender === 'm' ? '男性' : '女性'}
      />
      <UserFormItem
        itemName="メールアドレス"
        defaultValue={email}
        register={register}
        registerName="email"
        type="email"
        placeholder="mirine@global.com"
        error={errors?.email?.message}
      />
      <UserFormItem
        itemName="電話番号"
        defaultValue={phone}
        register={register}
        registerName="phone"
        type="number"
        placeholder="09012345678"
        error={errors?.phone?.message}
      />
      <UserFormItem
        itemName="郵便番号"
        defaultValue={postCode}
        register={register}
        registerName="postCode"
        type="number"
        placeholder="1234567"
        error={errors?.postCode?.message}
      />
      <UserFormItem
        itemName="住所"
        defaultValue={address}
        register={register}
        registerName="address"
        type="text"
        placeholder="東京都豊島区駒込１ー２ー３ミリネビル２０１"
        error={errors?.address?.message}
      />
    </>
  )
}
