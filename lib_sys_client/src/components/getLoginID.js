import axios from 'axios'

export const getLoginID = async () => {
  const response = await axios.get('http://localhost:3001/getID')
  return response.data.data
}
